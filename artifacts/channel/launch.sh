# # Launches the Orderer
# # Filedger Location specified in the YAML file.
# # You may override this
# export ORDERER_FILELEDGER_LOCATION=$HOME/ledgers/orderer/uni-net/ledger
# # Change this to control logs verbosity
# export FABRIC_LOGGING_SPEC=INFO
# #### Just in case this variable is not set ###
# export FABRIC_CFG_PATH=$PWD
# # Launch orderer
# orderer



# =================================================================================================================================
#!/bin/bash

# Launches the Orderer
# Filedger Location specified in the YAML file.
# You may override this
export ORDERER_FILELEDGER_LOCATION=$HOME/ledgers/orderer/uni-net/ledger
# Change this to control logs verbosity
export FABRIC_LOGGING_SPEC=INFO
#### Just in case this variable is not set ###
export FABRIC_CFG_PATH=$PWD
# Launch orderer
sudo /var/lib/docker/overlay2/3d25e64657f6cdad6373d11294129492d42db4c069e04f43e4fdae4b09f31a2f/diff/usr/local/bin/orderer
