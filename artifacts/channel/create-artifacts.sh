# set PATH so it includes HLF bin if it exists
if [ -d "/ws/hlf_assignment/fabric-samples/bin" ] ; then
PATH="/ws/hlf_assignment/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
# Delete existing artifacts
rm -rf ./crypto-config
rm financialnet_genesis.block financialnet_channel.tx


#Generate Crypto artifacts for organizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/

# Set the path to the configtx.yaml file
export FABRIC_CFG_PATH=$PWD

#generate the genesis block for the Banking consortium orderer
configtxgen -profile FinancialNetOrdererGenesis -channelID ordererchannel -outputBlock financialnet_genesis.block

# profile_name = FinancialNetOrdererGenesis
# channelID = ordererchannel
# file_name = financialnet_genesis.block

#generating the channel configuration transactio file
configtxgen -outputCreateChannelTx ./financialnet_channel.tx -profile FinancialNetChannel -channelID finetchannel


# #generating an anchor peer undate transaction
configtxgen -outputAnchorPeersUpdate BankOrgAnchors.tx -profile FinancialNetChannel -channelID finetchannel -asOrg BankOrg

